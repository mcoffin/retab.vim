function! retab#RetabIndents(should_print)
	if a:should_print
		echo retab#Mode()
	endif
	let saved_view = winsaveview()
	if &expandtab == 0
		execute '%s@^\( \{'.&ts.'}\)\+@\=repeat("\t", len(submatch(0))/'.&ts.')@'
	else
		execute '%s@^\(\t\)\+@\=repeat(" ", len(submatch(0))*'.&ts.')@'
	endif
	call winrestview(saved_view)
endfunction

function! retab#Mode()
	if &expandtab == 0
		return 'spaces -> tabs'
	else
		return 'tabs -> spaces'
	endif
endfunction
