if has('g:loaded_retab')
	finish
endif
let g:loaded_retab = 1

command! RetabIndents call retab#RetabIndents(0)
command! RetabMode echo retab#Mode()
nnoremap <silent><unique> <Plug>RetabIndents <Cmd>call retab#RetabIndents(1)<CR>
