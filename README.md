# retab.vim

Simple vim plugin for converting leading whitespace between tabs and spaces.

# Installation

Any normal vim package manager will do, but here, vundle is shown

```vim
Plugin 'https://gitlab.com/mcoffin/retab.vim
```

# Usage

`retab.vim` uses the `expandtab` setting to determine whether it should be converting to tabs or spaces, and the `tabstop` option for determining how "big" the tabs should be interpreted as.

## Commands

```vim
" displays which mode `retab` would operate in on this file.
:RetabMode
" converts leading whitespace between tabs and spaces (can be undone with normal vim undo/redo/etc).
:RetabIndents
```

## Keybinding

`retab.vim` adds a keybinding for `:RetabIndents` as `<Plug>RetabIndents`, so of you wanted to bind `\RT` to run the command, then you could add the following to your `~/.vimrc`.

```vim
nmap <silent> <Leader>RT <Plug>RetabIndents
```

```lua
vim.api.nvim_set_keymap('n', '<Leader>RT', '<Plug>RetabIndents', {})
```
